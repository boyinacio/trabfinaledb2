package br.ufrn.TAD;

import java.util.ArrayList;
import java.util.HashMap;

import br.ufrn.datastructure.UNode;

public class TreeUnionFind<T> {

	private ArrayList<UNode<T>> nodes;
	private HashMap<T, UNode<T>> mapa;

	public TreeUnionFind() {
		nodes = new ArrayList<UNode<T>>();
		mapa = new HashMap<T, UNode<T>>();
	}

	public void makeSet(T elem) {
		UNode<T> node = new UNode<T>();
		node.setData(elem);
		nodes.add(node);
		mapa.put(elem, node);
	}

	public T find(T elem) {
		UNode<T> N = mapa.get(elem);

		if (N.getPai().getData() != N.getData()) {
			N.setPai(mapa.get(find(N.getPai().getData())));
		}

		return N.getPai().getData();
	}

	public void union(T M, T N) {
		UNode<T> A = mapa.get(find(M));
		UNode<T> B = mapa.get(find(N));

		if (A == B)
			return;
		
		if (A.getRank() < B.getRank()) {
			A.setPai(B);
		} else {
			B.setPai(A);
			if (A.getRank() == B.getRank()) {
				A.setRank(A.getRank() + 1);
			}
		}
	}
	
}
