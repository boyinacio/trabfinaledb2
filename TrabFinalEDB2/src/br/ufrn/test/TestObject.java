package br.ufrn.test;

public class TestObject {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new TestObject().go();
	}

	public void go() {
		A x = new A();

		x.setA(1);
		x.setB(2);

		A y = x;
		y.setB(3);
		
		System.out.printf("A: %d - %d\n",x.getA(),x.getB());
		System.out.printf("B: %d - %d\n",y.getA(),y.getB());
	}

	public class A {
		int a;
		int b;

		public int getA() {
			return a;
		}

		public int getB() {
			return b;
		}

		public void setA(int a) {
			this.a = a;
		}

		public void setB(int b) {
			this.b = b;
		}
	}

}
