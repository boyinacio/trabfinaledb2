package br.ufrn.test;

import java.util.ArrayList;
import java.util.Random;

import br.ufrn.algorithm.Algorithm;
import br.ufrn.datastructure.Edge;

public class TestBruteForce {

	public static void main(String[] args) {
		 teste1();

//		int INI = 100;
//		int FIM = 10000;
//		int PASSO = 100;
//		int D = 2;
//		int MAX_COST;
//		
//		for (int i = INI; i <= FIM; i += PASSO) {
//			MAX_COST = 2 * ((i * (i + 1)) / 2);
//			System.out.println(i + "\t" + timeTest(i, D, MAX_COST));
//		}

//		 int N = 5;
//		 int D = 2;
//		 int MAX_COST = 2 * ((N * (N - 1)) / 2);
//		 randomTest(N, D, MAX_COST);

	}

	public static long timeTest(int N, int D, int MAX_COST) {
		ArrayList<Edge> edges = new ArrayList<Edge>();
		Random R = new Random();

		for (int i = 1; i <= N; i++) {
			for (int j = i + 1; j <= N; j++) {
				edges.add(new Edge(i, j, 1 + R.nextInt(MAX_COST)));
			}
		}

		long time = System.currentTimeMillis();
		Algorithm A = new Algorithm(edges);
		edges = A.bruteForce(edges, N, D);
		return System.currentTimeMillis() - time;
	}

	public static void randomTest(int N, int D, int MAX_COST) {
		ArrayList<Edge> edges = new ArrayList<Edge>();
		Random R = new Random();

		int c, sum = 0;
		for (int i = 1; i <= N; i++) {
			for (int j = i + 1; j <= N; j++) {
				c = 1 + R.nextInt(MAX_COST);
				sum += c;
				edges.add(new Edge(i, j, c));
			}
		}

		System.out.print("Custo de Todas as arestas: " + sum);

		System.out.println();

		Algorithm A = new Algorithm(edges);
		ArrayList<Edge> arestas = A.bruteForce(edges, N, D);
		int custo = 0;

		System.out.println("Número de arestas: " + arestas.size());
		for (Edge aresta : arestas) {
			System.out.println(aresta.toString());
			custo += aresta.getCost();
		}

		System.out.println("Custo total: " + custo);
	}

	public static void teste1() {
		ArrayList<Edge> edges = new ArrayList<Edge>();

		edges.add(new Edge(1, 2, 5));
		edges.add(new Edge(1, 3, 10));
		edges.add(new Edge(1, 4, 15));
		edges.add(new Edge(1, 5, 2));

		edges.add(new Edge(2, 3, 21));
		edges.add(new Edge(2, 4, 2));
		edges.add(new Edge(2, 5, 45));

		edges.add(new Edge(3, 4, 53));
		edges.add(new Edge(3, 5, 12));

		edges.add(new Edge(4, 5, 13));

		Algorithm A = new Algorithm(edges);
		ArrayList<Edge> arestas = A.bruteForce(edges, 5, 2);
		int custo = 0;

		System.out.println("Número de arestas: " + arestas.size());
		for (Edge aresta : arestas) {
			System.out.println(aresta.toString());
			custo += aresta.getCost();
		}
		
//		Node raiz = BuildTree.arvore(arestas, 2, 5);
//		BuildTree.printTree(raiz);

		System.out.println("Custo total: " + custo);
	}
	
}
