package br.ufrn.datastructure;

public class Edge implements Comparable<Edge> {

	private int A;
	private int B;
	private int cost;
	
	public Edge(int a, int b, int cost) {
		super();
		A = a;
		B = b;
		this.cost = cost;
	}
	
	public int getA() {
		return A;
	}

	public int getB() {
		return B;
	}

	public int getCost() {
		return cost;
	}

	public void setA(int a) {
		A = a;
	}

	public void setB(int b) {
		B = b;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}
	
	@Override
	public int compareTo(Edge o) {
		if(this.cost < o.cost){
			return -1;
		} else if(this.cost == o.cost){
			return 0;
		}
		return 1;
	}
		
	public String toString(){
		return A+"-"+B+" ("+cost+")";
	}

}
