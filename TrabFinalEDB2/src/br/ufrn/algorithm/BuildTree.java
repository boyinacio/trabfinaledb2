package br.ufrn.algorithm;


import java.util.ArrayList;

import com.sun.corba.se.spi.orbutil.fsm.Guard.Result;

import br.ufrn.datastructure.Edge;
import br.ufrn.datastructure.Node;

public class BuildTree {
	private static ArrayList<String> result;

	public BuildTree(){
		result = new ArrayList<String>();

	}

	public static Node arvore(ArrayList<Edge> resultEdges, int grau, int n){
		boolean [][] matriz = new boolean[n][n];
		ArrayList<Node> nos = new ArrayList<Node>();
		for (int i = 1; i <= n; i++) {
			Node no = new Node(grau);
			no.setLabel(i);
			nos.add(no);
		}
		for (int i = 0; i < resultEdges.size(); i++) {
			Edge E = resultEdges.get(i);
			matriz[E.getA()-1][E.getB()-1] = true;
			matriz[E.getB()-1][E.getA()-1] = true;
		}
		for (int i = 0; i < matriz.length; i++) {
			for (int j = i + 1; j < matriz.length; j++) {
				if(matriz[i][j] == true){
					if(nos.get(j).getPai() == null){
						nos.get(j).setPai(nos.get(i));
						nos.get(i).addFilho(nos.get(j));
					}else{
						nos.get(i).setPai(nos.get(j));
						nos.get(j).addFilho(nos.get(i));
					}

				}
			}
		}
		return nos.get(0);

	}

	public static void printTree(Node root){
		if(root == null) return;
		System.out.println("Nó: " + root.getLabel());
		ArrayList<Node> filhos = root.getFilhos();
		for (Node filho: filhos) {
			printTree(filho);
		}
	}

	public static ArrayList<String> printTree2(Node root){
		if(root == null) return result;
		result.add(" Nó: " + root.getLabel() + "\n");
		ArrayList<Node> filhos = root.getFilhos();
		for (Node filho: filhos) {
			printTree2(filho);
		}
		return result;
	}

}
