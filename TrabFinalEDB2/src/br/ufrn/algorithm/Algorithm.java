package br.ufrn.algorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import br.ufrn.TAD.TreeUnionFind;
import br.ufrn.datastructure.Edge;

public class Algorithm {

	private ArrayList<Edge> lista;
	private int quantArvoresValidas;

	public Algorithm(ArrayList<Edge> list) {
		lista = list;
		quantArvoresValidas = 0;
	}

	public ArrayList<Edge> bruteForce(ArrayList<Edge> edges, int N, int D) {
		quantArvoresValidas = 0;
		ArrayList<Edge> arestas = new ArrayList<Edge>();
		arestas.add(lista.get(0));
		ArrayList<Edge> result = go(new ArrayList<Edge>(), arestas, N, D, 0);
		return result;
	}

	private ArrayList<Edge> go(ArrayList<Edge> A, ArrayList<Edge> B, int N,
			int D, int pos) {

		if (A.size() < (N - 1)) {
			ArrayList<Edge> copyA = new ArrayList<Edge>(A);
			if ((pos + 1) < lista.size()) {
				copyA.add(lista.get(pos + 1));
				A = go(A, copyA, N, D, pos + 1);
			}
		}

		if (B.size() < (N - 1)) {
			ArrayList<Edge> copyB = new ArrayList<Edge>(B);
			if ((pos + 1) < lista.size()) {
				copyB.add(lista.get(pos + 1));
				B = go(B, copyB, N, D, pos + 1);
			}
		}

		int custoA = getCusto(A, N, D);
		int custoB = getCusto(B, N, D);

		return (custoA == -1) ? B : ((custoB == -1) ? A
				: ((custoA < custoB) ? A : B));
	}

	private int getCusto(ArrayList<Edge> edges, int N, int D) {
		Object[] values = generateUnionFindAndGradeMap(edges, N);

		TreeUnionFind<Integer> unionFind = (TreeUnionFind<Integer>) values[0];
		HashMap<Integer, Integer> mapa = (HashMap<Integer, Integer>) values[1];

		if (validaConfig(unionFind, mapa, N, D)) {
			int custo = 0;
			for (Edge E : edges) {
				custo += E.getCost();
			}
			quantArvoresValidas++;
			return custo;
		}

		return -1;
	}

	private Object[] generateUnionFindAndGradeMap(ArrayList<Edge> edges, int N) {
		TreeUnionFind<Integer> UF = new TreeUnionFind<Integer>();
		HashMap<Integer, Integer> MAP = new HashMap<Integer, Integer>();

		for (int i = 1; i <= N; i++) {
			UF.makeSet(i);
			MAP.put(i, 0);
		}

		for (Edge E : edges) {
			UF.union(E.getA(), E.getB());
			MAP.put(E.getA(), MAP.get(E.getA()) + 1);
			MAP.put(E.getB(), MAP.get(E.getB()) + 1);
		}

		Object[] values = { UF, MAP };

		return values;
	}

	private boolean validaConfig(TreeUnionFind<Integer> sets,
			HashMap<Integer, Integer> nodoGrau, int N, int D) {
		// Verifica se há algum nodo desconectado
		for (int i = 1; i < N; i++) {
			for (int j = i + 1; j <= N; j++) {
				if (sets.find(i) != sets.find(j)) {
					return false;
				}
			}
		}

		// Verifica se os graus dos nodos estão abaixo do limite
		for (Iterator it = nodoGrau.entrySet().iterator(); it.hasNext();) {
			Map.Entry pairs = (Map.Entry) it.next();
			Integer grau = (Integer) pairs.getValue();
			if (grau > D) {
				return false;
			}
		}

		return true;
	}

	public int getQuantArvoresValidas() {
		return quantArvoresValidas;
	}
}
