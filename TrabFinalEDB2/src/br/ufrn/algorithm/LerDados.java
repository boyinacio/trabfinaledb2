package br.ufrn.algorithm;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import br.ufrn.datastructure.Edge;

public class LerDados {

	public static Object[] lerDados(File entrada){

		Scanner leitor = null;
		try {
			leitor = new Scanner(entrada);
		} catch (FileNotFoundException e) {
			System.out.println("Arquivo não encontrado!");
			tentarDeNovo();
		}

		ArrayList<Edge> listaArestas = new ArrayList<Edge>();
		int n, d;

		n = leitor.nextInt();
		d = leitor.nextInt();

		for (int i = 1; i <= n; i++){
			for (int j = i+1; j <= n; j++) {
				listaArestas.add(new Edge(i,j,leitor.nextInt()));
			}
		}

		leitor.close();

		Object[] data = {n,d,listaArestas};

		return data;
	}

	private static File tentarDeNovo(){
		String path;
		File f;
		Scanner leitor = new Scanner(System.in);
		System.out.println("Informe o caminho do arquivo: ");
		path = leitor.nextLine();
		f = new File(path);
		leitor.close();
		return f;
	}

}
