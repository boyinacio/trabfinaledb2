package br.ufrn.algorithm;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import br.ufrn.datastructure.Edge;
import br.ufrn.datastructure.Node;


public class SalvarDadosArquivo {

	private FileWriter arq;
	private PrintWriter gravarArq;
	private BuildTree resultado;
	private Algorithm sizeTree2;
	private ArrayList<String> arquivo = new ArrayList<String>();
	private ArrayList<String> edges = new ArrayList<String>();
	private int custo = 0;


	public void saveData(Node root, ArrayList<Edge> arestas, long time, int sizeTree){
		//sizeTree2 = new Algorithm(arestas);
		resultado = new BuildTree();
		arquivo = resultado.printTree2(root);
		edges = saveDataEdges(root, arestas);
		custo = custo(arestas, custo);
		//sizeTree = sizeTree2.getQuantArvoresValidas();


		try {
			arq = new FileWriter("Arquivos_Saida/ArquivoSaida");
			gravarArq  = new PrintWriter(arq);
			gravarArq.print("Numero de arestas: " + arestas.size() + "\n");
			gravarArq.print("Quantidade de árvores válidas geradas: " + sizeTree + "\n");
			for (int j = 0; j < edges.size(); j++) {
				gravarArq.printf(edges.get(j));
			}

			for (int i = 0; i < arquivo.size(); i++) {
				gravarArq.printf(arquivo.get(i));
			}
			gravarArq.println("Custo total: " + custo);
			gravarArq.print("Duração do algoritmo: " + time + " milissegundos");
			//gravarArq.printf(arquivo);
			arq.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public ArrayList<String> saveDataEdges(Node root, ArrayList<Edge> arestas){
		resultado = new BuildTree();
		arquivo = resultado.printTree2(root);
		for (Edge aresta : arestas) {
			edges.add(aresta.toString() + "\n");
		}
		return edges;
	}


	public int custo(ArrayList<Edge> arestas, int custo){
		for (Edge aresta : arestas) {
			custo += aresta.getCost();
		}
		return custo;
	}


}
