Universidade Federal do Rio Grande do Norte

Instituto Metrópole Digital

Bacharelado em Tecnologia da Informação

IMD0039 - Estruturas de Dados Básicas II

IMD0040 - Linguagem de Programação II

README DO PROJETO UNIFICADO - 3ª UNIDADE ­ 2014.2 

SOBRE
=======
Este projeto consiste na implementação algorítmica requisitada pelo projeto
unificado da 3ª unidade das disciplinas IMD0039 e IMD0040, no qual se pede
para construir um programa que, ao receber um conjunto de arestas, referentes
às distâncias das casas dos moradores do bairro de Parnatal, produza \(e retorne\)
uma árvore de custo mínimo referente ao layout da rede cabeada por fibra óptica
que será instalada no referido bairro.

AUTORES
=======
* Inácio Gomes Medeiros
* Ramon Santos Malaquias
* Silas Tiago de Medeiros

UFRN-IMD-Bacharelado em Tecnologia da Informação

INSTALL
=======
* Abra o projeto no Eclipse
 1. Abra o Eclipse IDE para Desenvolvedores Java. Se você não o possui, faça o download
neste [link](https://www.eclipse.org/downloads/packages/eclipse-ide-java-and-dsl-developers/lunasr1)
 2. No Eclipse, clique em "File ~> New ~> Java Project"
 3. Na janela que se abre, desmarque "Use Default Location", e clique em "Browse" para selecionar
a pasta dos fontes
 4. Clique em "Finish" abrir o projeto

* Gere o jar referente ao aplicativo
 1. Execute pelo menos uma vez no eclipse o projeto, clicando com o botão direito do mouse no projeto e cliando
em "Run As ~> Java Application"
 2. Clique em "File ~> Export"
 3. Na janela que se abre, clique em "Java ~> Runnable JAR file", e, em seguida, em "Next"
 4. Selecione a classe "Main" do projeto do trabalho em "Launch Configuration"
 5. Selecione o diretório onde o arquivo jar ficará armazenado clicando no botão "Browser" em 
"Export Destination" e escolhe um nome para o arquivo jar
 6. Clique em "Finish"

* Executando o arquivo jar
 1. Abra o terminal
 2. Vá até o diretório onde se encontra o arquivo <nome_escolhido>.jar
 3. Digite o comando "java -jar <nome_escolhido>.jar" e aperte enter

COPYING
========
Este projeto está licenciado sob a licença [GLPv3](http://www.gnu.org/copyleft/gpl.html)
